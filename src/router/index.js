import {createRouter, createWebHistory} from "vue-router";
import HomeView from "@/views/HomeView.vue";
import ProjectsView from "@/views/ProjectsView.vue";
import ProjectDetailView from "@/views/ProjectDetailView.vue";

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: HomeView
        },
        {
            path: '/projects',
            component: ProjectsView
        },
        {
            path: '/projects/:projectId',
            component: ProjectDetailView
        }
    ]
})