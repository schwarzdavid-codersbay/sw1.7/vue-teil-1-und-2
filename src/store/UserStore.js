import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";

export const useUserStore = defineStore('users', () => {
    const users = ref([])
    const USER_API_URL = 'https://vjsbtjzvcc.user-management.asw.rest/api/users'

    async function loadAllUsers() {
        const userResponse = await axios.get(USER_API_URL)
        users.value = userResponse.data
    }

    async function ensureUser(userId) {
        const existingUser = users.value.find(user => user.userId === userId)
        if(!existingUser) {
            const userResponse = await axios.get(`${USER_API_URL}/${userId}`)
            users.value.push(userResponse.data)
        }
    }

    return {
        users,
        loadAllUsers,
        ensureUser
    }
})

/*export const useUserOptionStore = defineStore('users-options', {
    state: () => ({
        users: []
    }),
    actions: {
        loadAllUsers: async function () {
            const userResponse = await axios.get('https://sbvpxvrxio.user-management.asw.rest/api/users')
            this.users = userResponse.data
        }
    }
})*/