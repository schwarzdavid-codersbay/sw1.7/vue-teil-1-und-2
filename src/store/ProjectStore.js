import {defineStore} from "pinia";
import {ref} from "vue";

export const useProjectStore = defineStore('project-store', () => {
    const projects = ref([
        {
            projectId: 1,
            name: "Project Management",
            image: 'https://images.pexels.com/photos/19312037/pexels-photo-19312037.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
            price: 1750.5
        },
        {
            projectId: 2,
            name: 'Kanban Template',
            image: 'https://images.pexels.com/photos/19125348/pexels-photo-19125348/free-photo-of-meer-seebrucke-seelandschaft-gelander.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'
        },
        {
            projectId: 3,
            name: 'Simple Project Board'
        },
        {
            projectId: 4,
            name: 'Remote Team Hub'
        }
    ])
    const extendedProjects = ref([])

    function addProject(project) {
        projects.value.push(project)
    }

    return {
        projects,
        extendedProjects,
        addProject
    }
})