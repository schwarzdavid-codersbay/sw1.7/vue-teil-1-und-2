import {defineStore} from "pinia";
import {ref} from "vue";

export const useCounterStore = defineStore('counter-store', () => {
    const timesClicked = ref(0)

    return {timesClicked}
})